import React from 'react';
import Base from '../components/base';
import TitleBar from '../components/titlebar';
import Dashboard from '../modules/dashboard/dashboard';

const Index = () => {
  const routes = [
    {
      path: '/',
      name: 'Home',
    },
  ];

  return (
    <div className="app">
      <Base title="Dashboard">
        <TitleBar title="Dashboard" routes={routes} />
        <div className="row m-0">
        <Dashboard />
      </div>
      </Base>
    </div>
  );
};

export default Index;
